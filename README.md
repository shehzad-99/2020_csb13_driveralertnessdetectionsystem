# 2020_CSB13_DriverAlertnessDetectionSystem

**Team Members**
- Mohamed Shehzad Hanif (22)
- Mohammed T (23)
- N Adarsh (27)
- N Akshay (28)

[Project Status Video Update](https://drive.google.com/drive/folders/1QPT_jHB5ce1AthlH4r-HbfwgP83rv5rC?usp=sharing)

[Dataset Link](https://www.kaggle.com/shehzadhanif/eyes-open-closed-dataset)

haar cascade 
 [for face -](https://github.com/Itseez/opencv/blob/master/data/haarcascades/haarcascade_frontalface_default.xml)
 [for eye -](https://github.com/Itseez/opencv/blob/master/data/haarcascades/haarcascade_eye.xml)


[Akshay Contribution](https://drive.google.com/file/d/1xzNYA9RwAjEiKdlLZIHAP3UnSm-0EX_F/view?usp=sharing)

[Adharsh Contribution](https://drive.google.com/file/d/1NYjB273pr7I1e_SPUrhqVCqNDOamtRvh/view?usp=sharing)

[Mohammed Contribution](https://drive.google.com/file/d/1KxA7vdXjBxRQA2NBzVBuyOU_JAqQIo7i/view?usp=sharing)

[Shehzad Contribution](https://drive.google.com/file/d/1epr3OWVnITUMVuCDioSZGNlM2SXQ48kS/view?usp=sharing)
